<?php

namespace App\Http\Controllers;

use App\Models\DompetStatus;
use App\Models\KategoriStatus;
use App\Models\MDompet;
use App\Models\MKategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class MsKategori extends Controller
{

    public function index()
    {
        $kategori       = MKategori::all();
        $semuaData      = MKategori::all()->count();
        $dataAktif      = MKategori::where('status_id','1')->count();
        $dataTidakAktif = MKategori::where('status_id','2')->count();

        return view('master.kategori.index',
        compact(
    'kategori',
  'semuaData',
             'dataAktif',
             'dataTidakAktif'
        ));
    }


    public function create()
    {
        $kategoriStatus = KategoriStatus::orderBy('name','asc')->get();
        return view('master.kategori.create', compact('kategoriStatus'));
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => ['required', 'min:5'],
            'deskripsi' => ['required', 'max:100'],
        ]);

        DB::beginTransaction();
        try {
            MKategori::create([
                'status_id'     => $request->status,
                'name'          => $request->nama,
                'deskripsi'     => $request->deskripsi,
            ]);
            Session::flash('status', 'success');
            Session::flash('messages', 'Success create data!');
            DB::commit();
            return redirect('/master-kategori');
        } catch (\Throwable $th) {
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail create data!');
            DB::rollback();
            return back();
        }
    }


    public function show($id)
    {
        $id = Crypt::decrypt($id);
        $kategori = MKategori::find($id);
        $kategoriStatus = KategoriStatus::all();
        return view('master.kategori.show',compact('kategoriStatus','kategori'));
    }


    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $kategori = MKategori::find($id);
        $kategoriStatus = KategoriStatus::all();
        return view('master.kategori.edit',compact('kategori','kategoriStatus'));
    }


    public function update(Request $request, $id)
    {
        $id = Crypt::decrypt($id);
        $kategori = MKategori::find($id);
        $this->validate($request,[
            'nama'  => ['required', 'min:5', 'string'],
            'deskripsi' => ['required', 'max:100'],
        ]);
        DB::beginTransaction();
        try {
            $kategori->update([
                'status_id'     => $request->status,
                'name'          => $request->nama,
                'deskripsi'     => $request->deskripsi,
            ]);
            Session::flash('status', 'success');
            Session::flash('messages', 'Success update data!');
            DB::commit();
            return redirect('/master-kategori');
        } catch (\Throwable $th) {
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail update data!');
            DB::rollback();
            return back();
        }
    }

    public function aktif($id)
    {
        $id = Crypt::decrypt($id);
        $kategoriId = MKategori::find($id);
        $kategoriId->update([
            'status_id' => '1',
        ]);
        return redirect('/master-kategori');
    }

    public function tidakAktif($id)
    {
        $id = Crypt::decrypt($id);
        $kategoriId = MKategori::find($id);
        $kategoriId->update([
            'status_id' => '2',
        ]);
        return redirect('/master-kategori');
    }

}
