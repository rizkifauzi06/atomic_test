<?php

namespace App\Http\Controllers;

use App\Models\DompetStatus;
use App\Models\MDompet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class MsDompet extends Controller
{

    public function index()
    {
        $dompet         = MDompet::all();
        $semuaData      = MDompet::all()->count();
        $dataAktif      = MDompet::where('status_id','1')->count();
        $dataTidakAktif = MDompet::where('status_id','2')->count();

        return view('master.dompet.index',
        compact(
    'dompet',
  'semuaData',
             'dataAktif',
             'dataTidakAktif'
        ));
    }


    public function create()
    {
        $dompetStatus = DompetStatus::all();
        return view('master.dompet.create', compact('dompetStatus'));
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'nama' => ['required', 'min:5'],
            'deskripsi' => ['required', 'max:100'],
        ]);

        DB::beginTransaction();
        try {
            MDompet::create([
                'status_id'     => $request->status,
                'name'          => $request->nama,
                'referensi'     => $request->referensi,
                'deskripsi'     => $request->deskripsi,
            ]);
            Session::flash('status', 'success');
            Session::flash('messages', 'Success create data!');
            DB::commit();
            return redirect('/master-dompet');
        } catch (\Throwable $th) {
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail create data!');
            DB::rollback();
            return back();
        }
    }


    public function show($id)
    {
        $id = Crypt::decrypt($id);
        $dompet = MDompet::find($id);
        $dompetStatus = DompetStatus::all();
        return view('master.dompet.show',compact('dompet','dompetStatus'));
    }


    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $dompet = MDompet::find($id);
        $dompetStatus = DompetStatus::all();
        return view('master.dompet.edit',compact('dompet','dompetStatus'));
    }


    public function update(Request $request, $id)
    {
        $id = Crypt::decrypt($id);
        $dompet = MDompet::find($id);
        $this->validate($request,[
            'nama'  => ['required', 'min:5', 'string'],
            'deskripsi' => ['required', 'max:100'],
        ]);
        DB::beginTransaction();
        try {
            $dompet->update([
                'status_id'     => $request->status,
                'name'          => $request->nama,
                'referensi'     => $request->referensi,
                'deskripsi'     => $request->deskripsi,
            ]);
            Session::flash('status', 'success');
            Session::flash('messages', 'Success update data!');
            DB::commit();
            return redirect('/master-dompet');
        } catch (\Throwable $th) {
            Session::flash('status', 'danger');
            Session::flash('messages', 'Fail update data!');
            DB::rollback();
            return back();
        }
    }

    public function aktif($id)
    {
        $id = Crypt::decrypt($id);
        $dompetId = MDompet::find($id);
        $dompetId->update([
            'status_id' => '1',
        ]);
        return redirect('/master-dompet');
    }

    public function tidakAktif($id)
    {
        $id = Crypt::decrypt($id);
        $dompetId = MDompet::find($id);
        $dompetId->update([
            'status_id' => '2',
        ]);
        return redirect('/master-dompet');
    }

}
