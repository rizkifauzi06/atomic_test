<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KategoriStatus extends Model
{
    use HasFactory;

    protected $table = 'kategori_status';

    protected $fillable = [
        'name',
        'created_at',
        'updated_at',
    ];

    public function master_kategori()
    {
        return $this->hasOne(MKategori::class, 'status_id','id');
    }

}
