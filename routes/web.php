<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\MsDompet;
use \App\Http\Controllers\MsKategori;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//ROUTE DOMPET
Route::resource('master-dompet', MsDompet::class);
Route::post('master-dompet/aktif/{id}', [MsDompet::class, 'aktif']);
Route::post('master-dompet/tidak-aktif/{id}', [MsDompet::class, 'tidakAktif']);
//END ROUTE DOMPET

//ROUTE KATEGORI
Route::resource('master-kategori', MsKategori::class);
Route::post('master-kategori/aktif/{id}', [MsKategori::class, 'aktif']);
Route::post('master-kategori/tidak-aktif/{id}', [MsKategori::class, 'tidakAktif']);
//END ROUTE KATEGORI


