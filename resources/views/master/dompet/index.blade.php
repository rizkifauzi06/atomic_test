@extends('layouts.master')

@push('title','Master Dompet')

@push('title-breadcrumb', 'Dompet')

@push('script-js-head')
@endpush

@section('content')

<div class="nk-content ">
    <div class="container-fluid">
        <div class="nk-content-inner">
            <div class="nk-content-body">
                <div class="nk-block-head nk-block-head-sm">
                    <nav>
                        <ul class="breadcrumb breadcrumb-pipe">
                            <li class="breadcrumb-item"><a href="#">Master</a></li>
                            <li class="breadcrumb-item active">Dompet</li>
                        </ul>
                    </nav>
                    <div class="nk-block-between">
                        <div class="nk-block-head-content">
                            <h3 class="nk-block-title page-title">{{ __('Dompet') }}</h3>
                        </div>
                        @if (session('success'))
                            <div class="mt-2">
                                <div class="alert alert-success p-1">
                                    {{ session('success') }}
                                </div>
                            </div>
                        @endif

                        @if (session('error'))
                            <div class="mt-2">
                                <div class="alert alert-danger p-1">
                                    {{ session('error') }}
                                </div>
                            </div>
                        @endif
                        <div class="nk-block-head-content">
                            <div class="toggle-wrap nk-block-tools-toggle">
                                <a class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu">
                                    <em class="icon ni ni-more-v"></em>
                                </a>
                                <div class="toggle-expand-content" data-content="pageMenu">
                                    <div class="row">
                                        <div class="nk-block-tools g-3">
                                            <div class="nk-block-tools-opt mr-1">
                                                <a href="{{ URL('/master-dompet/create') }}" type="button" class="btn btn-primary">
                                                    <em class="icon ni ni-plus-sm"></em>
                                                    <span>{{ __('Tambah Dompet') }}</span>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="nk-block-tools g-3">
                                            <div class="nk-block-tools-opt">
                                                <div class="dropdown">
                                                    <a href="#" class="btn btn-success" data-toggle="dropdown" aria-expanded="false">
                                                        <span>Semua ( {{ $semuaData }} )</span>
                                                        <em class="icon ni ni-chevron-down"></em>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-auto mt-1" style="">
                                                        <ul class="link-list-plain">
                                                            <li>
                                                                <a href="#">Aktif ( {{ $dataAktif }} )</a>
                                                            </li>
                                                            <li>
                                                                <a href="#">Tidak Aktif ( {{ $dataTidakAktif }} )</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="nk-block nk-block-lg">
                    <div class="nk-block-head">
                        <div class="nk-block-head-content">
                            <h4 class="nk-block-title">Data Dompet</h4>
                            <div class="nk-block-des">
                                <p>Tabel data dompet </p>
                            </div>
                        </div>
                    </div>
                    <!--Alert-->
                    <table class="datatable-init nowrap nk-tb-list is-separate" data-auto-responsive="false">
                        <thead>
                        <tr class="nk-tb-item nk-tb-head">
                            <th class="nk-tb-col nk-tb-col-check"><span class="sub-text">No</span></th>
                            <th class="nk-tb-col"><span class="sub-text">Nama</span></th>
                            <th class="nk-tb-col"><span class="sub-text">Referensi</span></th>
                            <th class="nk-tb-col"><span class="sub-text">Deskripsi</span></th>
                            <th class="nk-tb-col"><span class="sub-text">Status</span></th>
                            <th class="nk-tb-col nk-tb-col-action"><span class="sub-text">Aksi</span></th>
                        </tr><!-- .nk-tb-item -->
                        </thead>
                        <tbody>
                        @foreach ($dompet as $d)
                            <tr class="nk-tb-item">
                                <td class="nk-tb-col nk-tb-col-check">{{$loop->iteration}}</td>
                                <td class="nk-tb-col">{{$d->name}}</td>
                                <td class="nk-tb-col">{{$d->referensi}}</td>
                                <td class="nk-tb-col">{{$d->deskripsi}}</td>
                                <td class="nk-tb-col">{{$d->dompet_status->name}}</td>
                                <td class="nk-tb-col nk-tb-col-action">
                                    <div class="dropdown">
                                        <a class="text-soft dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown" aria-expanded="false">
                                            <em class="icon ni ni-more-h"></em>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-xs" style="">
                                            <ul class="link-list-plain">
                                                <li>
                                                    <a href="{{URL('/master-dompet/'.Illuminate\Support\Facades\Crypt::encrypt($d->id))}}"><em class="icon ni ni-eye"></em> Detail</a>
                                                </li>
                                                <li>
                                                    <a href="{{URL('/master-dompet/'.Illuminate\Support\Facades\Crypt::encrypt($d->id).'/edit')}}" ><em class="icon ni ni-edit"></em> Ubah</a>
                                                </li>
                                                @if($d->status_id != 1)
                                                    <li>
                                                        <form action="{{URL('/master-dompet/aktif/'.Illuminate\Support\Facades\Crypt::encrypt($d->id))}}" class="form-horizontal" method="POST">
                                                            @csrf
                                                            @method('POST')
                                                            <button type="submit" class="btn btn-sm"><em class="icon ni ni-check"></em> Aktif</button>
                                                        </form>
                                                    </li>
                                                @else
                                                    <li>
                                                        <form action="{{URL('/master-dompet/tidak-aktif/'.Illuminate\Support\Facades\Crypt::encrypt($d->id))}}" class="form-horizontal" method="POST">
                                                            @csrf
                                                            @method('POST')
                                                            <button type="submit" class="btn btn-sm"><em class="icon ni ni-sign-xrp"></em>&nbsp; Tidak Aktif</button>
                                                        </form>
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- Child rows (show extra / detailed information) -->
@endsection

@push('script-js')
<script src="{{URL('assets/adminBite/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script src="{{URL('assets/adminBite/dist/js/pages/datatable/datatable-api.init.js')}}"></script>
@endpush
