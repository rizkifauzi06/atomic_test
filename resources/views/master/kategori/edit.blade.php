@extends('layouts.master')

@push('title','Master Kategori')

@push('title-breadcrumb', 'Kategori')

@push('script-js-head')
@endpush

@section('content')

    <div class="nk-content ">
        <div class="container-fluid">
            <div class="nk-content-inner">
                <div class="nk-content-body">
                    <div class="nk-block-head nk-block-head-sm">
                        <nav>
                            <ul class="breadcrumb breadcrumb-pipe">
                                <li class="breadcrumb-item">Master</li>
                                <li class="breadcrumb-item">Kategori</li>
                                <li class="breadcrumb-item active">edit</li>
                            </ul>
                        </nav>
                        <div class="nk-block-between">
                            <div class="nk-block-head-content">
                                <h3 class="nk-block-title page-title">{{ __('Kategori') }}</h3>
                            </div>
                            @if (session('success'))
                                <div class="mt-2">
                                    <div class="alert alert-success p-1">
                                        {{ session('success') }}
                                    </div>
                                </div>
                            @endif

                            @if (session('error'))
                                <div class="mt-2">
                                    <div class="alert alert-danger p-1">
                                        {{ session('error') }}
                                    </div>
                                </div>
                            @endif
                            <div class="nk-block-head-content">
                                <div class="toggle-wrap nk-block-tools-toggle">
                                    <a class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu">
                                        <em class="icon ni ni-more-v"></em>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="nk-block nk-block-lg">
                        <div class="card">
                            <form action="{{URL('/master-kategori/'.Illuminate\Support\Facades\Crypt::encrypt($kategori->id))}}" class="form-horizontal" method="POST">
                                @csrf
                                @method('PUT')
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="form-label" for="full-name-1">Nama</label>
                                                <div class="form-control-wrap">
                                                    <input name="nama" type="text" value="{{old('nama', $kategori->name)}}" onblur="checkLength(this)" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mt-1">
                                            <div class="form-group">
                                                <label class="form-label" for="full-name-1">Deskripsi</label>
                                                <div class="form-control-wrap">
                                                    <textarea name="deskripsi" class="form-control">
                                                        {{old('deskripsi', $kategori->deskripsi)}}
                                                    </textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mt-1">
                                            <div class="form-group">
                                                <label class="form-label" for="full-name-1">Status</label>
                                                <div class="form-control-wrap">
                                                    <select name="status" class="form-control">
                                                        @foreach($kategoriStatus as $ks)
                                                            <option value="{{ $ks->id }}" {{($kategori->status_id == $ks->id)? 'selected':''}}>{{ $ks->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-info mt-5"> Simpan</button>
                                    <a href="{{ URL('/master-kategori') }}" class="btn btn-warning mt-5"> Kembali</a>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- End Row -->
@endsection

@push('script-js')
@endpush
