<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    @include("layouts.component.head")
</head>

<body class="nk-body bg-lighter npc-default has-sidebar ">
<div class="nk-app-root">
    <div class="nk-main ">
        @include("layouts.component.side-bar")
        <div class="nk-wrap ">
            @include("layouts.component.header")
            @yield("content")
            @include("layouts.component.footer")
        </div>
    </div>
</div>
@include("layouts.component.script")
</body>

</html>
