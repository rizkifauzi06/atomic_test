<base href="../">
<meta charset="utf-8">
<meta name="author" content="Softnio">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="A powerful and conceptual apps base dashboard template that especially build for developers and programmers.">
<!-- Fav Icon  -->
<link rel="shortcut icon" href="{{ URL("dashlite/images/favicon.png") }}">
<!-- Page Title  -->
<title>@stack('title')</title>
<!-- StyleSheets  -->
<link rel="stylesheet" href="{{ URL("dashlite/assets/css/dashlite.css?ver=2.4.0") }}">
<link id="skin-default" rel="stylesheet" href="{{ URL("dashlite/assets/css/theme.css?ver=2.4.0") }}">
